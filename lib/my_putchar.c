/*
** my_putchar.c for lib in /home/mariet_v//celem/minitalk/lib
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 16:03:47 2013 valentin mariette
** Last update Tue Mar 19 16:04:11 2013 valentin mariette
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
