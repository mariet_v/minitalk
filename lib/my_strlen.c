/*
** exo3.c for my_strlen in /home/mariet_v//day4
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Oct  4 10:19:22 2012 valentin mariette
** Last update Sun Mar 10 15:08:13 2013 valentin mariette
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i++;
    }
  return (i);
}
