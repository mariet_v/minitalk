/*
** main.c for do-op in /home/mariet_v//day11/do-op
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Oct 22 11:06:50 2012 valentin mariette
** Last update Sun Mar 17 13:06:11 2013 valentin mariette
*/

int	get_the_nbr(char *str, char i, char sign, int sum)
{
  while (1 <= my_strlen(str))
    {
      if (str[i] > 47 && str[i] < 58)
	{
	  if (sum > 214748364 && (sum == 214748364 && (str[i] - 48) > 7)
	      || sum < -214748364 && (sum == -214748364 && (str[i] - 48) > 8))
	    return (0);
	  sum = sum * 10 + str[i] - 48;
	}
      else if (str[i] ==  45)
	sign = -1;
      else if (str[i] == 43)
	sign = 1;
      else
	{
	  sum = sign * sum;
	  return (sum);
	}
      i++;
    }
  return (sum);
}

int	my_getnbr(char *str)
{
  return (get_the_nbr(str, 0, 1, 0));
}
