/*
** server.h for minitalk-server in /home/mariet_v//celem/minitalk/server
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 15:57:34 2013 valentin mariette
** Last update Tue Mar 19 15:58:37 2013 valentin mariette
*/

#ifndef SERVER_H_
# define SERVER_H_

void    get_signal(int signal);

#endif
