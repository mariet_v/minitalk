/*
** main.c for minitalk-server in /home/mariet_v//celem/minitalk/server
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 14:51:17 2013 valentin mariette
** Last update Wed Mar 20 17:18:33 2013 valentin mariette
*/

#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include "server.h"

int	main(int argc, char **argv)
{
  my_put_nbr(getpid());
  my_putchar('\n');
  signal(SIGUSR1, get_signal);
  signal(SIGUSR2, get_signal);
  while (42)
    {
      sleep(1);
    }
}

void    get_signal(int signal)
{
  stock_bit(signal);
}
