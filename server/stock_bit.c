/*
** stock_bit.c for minitalk-server in /home/mariet_v//celem/minitalk/lib
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 16:07:44 2013 valentin mariette
** Last update Wed Mar 20 17:23:56 2013 valentin mariette
*/

#include <stdlib.h>
#include <signal.h>
#include "server.h"

void	stock_bit(int signal)
{
  static char c;
  static char nbr;

  c = c << 1;
  if (signal == SIGUSR2)
    c += 1;
  nbr++;
  if (nbr == 8)
    {
      nbr = 0;
      if (c == 96)
	c = 32;
      my_putchar(c);
      c = 0;
    }
}
