/*
** main.c for minitalk-client in /home/mariet_v//celem/minitalk/client
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 14:51:44 2013 valentin mariette
** Last update Wed Mar 20 17:19:40 2013 valentin mariette
*/

#include <sys/types.h>
#include <signal.h>
#include "client.h"

int	main(int argc, char **argv)
{
  int	server;
  int	i;
  int	j;
  char	msg[8];

  i = 0;
  if (argc < 3)
    error(1);
  server = my_getnbr(argv[1]);
  while (i < my_strlen(argv[2]))
    {
      j = 7;
      take_letter(argv[2][i], msg);
      while (j >= 0)
	{
	  if (msg[j] == 0)
	    kill(server, SIGUSR1);
	  else if (msg[j] == 1)
	    kill(server, SIGUSR2);
	  usleep(2000);
	  j--;
	}
      i++;
    }
  my_putstr("Done.\n");
}

char	*take_letter(char c, char *msg)
{
  int	i;

  i = 0;
  while (c > 0)
    {
      msg[i] = c % 2;
      c /= 2;
      i++;
    }
  return (msg);
}
