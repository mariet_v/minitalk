/*
** error.c for minitalk-client in /home/mariet_v//celem/minitalk/client
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Mar 19 16:31:50 2013 valentin mariette
** Last update Tue Mar 19 16:35:17 2013 valentin mariette
*/

#include <stdlib.h>

void	error(int flag)
{
  if (flag == 1)
    my_putstr("Erreur de parametre\n");
  exit(-1);
}
