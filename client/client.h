/*
** client.h for client-minitalk in /home/mariet_v//celem/minitalk/client
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Wed Mar 20 16:55:24 2013 valentin mariette
** Last update Wed Mar 20 17:10:04 2013 valentin mariette
*/

#ifndef CLIENT_H_
# define CLIENT_H_

char    *take_letter(char c, char *msg);

#endif
