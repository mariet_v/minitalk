##
## Makefile for libmy in /afs/epitech.net/users/all/mariet_v/rendu/lib/my
## 
## Made by valentin mariette
## Login   <mariet_v@epitech.net>
## 
## Started on  Mon Oct 22 10:29:25 2012 valentin mariette
## Last update Wed Mar 20 17:15:26 2013 valentin mariette
##

SRC_CLIENT= 	client/main.c \
		client/error.c \
		lib/my_putchar.c \
		lib/my_putstr.c \
		lib/my_getnbr.c \
		lib/my_strlen.c \
		lib/my_put_nbr.c

SRC_SERVER=	server/main.c \
		server/stock_bit.c \
		lib/my_putchar.c \
		lib/my_put_nbr.c

OBJ_CLIENT=	$(SRC_CLIENT:.c=.o)

OBJ_SERVER=	$(SRC_SERVER:.c=.o)

all:	client server

client:	$(OBJ_CLIENT)
	cc -o client/client $(OBJ_CLIENT)

server: $(OBJ_SERVER)
	cc -o server/server $(OBJ_SERVER)

clean: 
	rm -f $(OBJ_CLIENT)
	rm -f $(OBJ_SERVER)

fclean:	clean
	rm -f client/client server/server

re: fclean all

love:	
	@echo 'not war?'